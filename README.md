# Chess Game

Chess game is an app to calculate possibles moviments for Knight on a chessboard. We use an api with express to call the requests. It's possible to see the website[https://chessgameapp.herokuapp.com/].

## Structure

- React
- TypeScript
- Webpack
- Mobx
- Axios
- SCSS

## Scripts

- npm run install (install all the dependencies of your app)
- npm run build (build your app with statics files)
- npm run dev (locally run your app)
- npm run start (to deploy your app and run node without hot reload)

# Organization

- containers (folders to hold functions with state)
- components (to hold dumb componentes, without state)
- stores (logic of the app, authentication and moviments)
- assets (store images)
- types (store types of application, like the api type)
- utils (functions and variables that are to be reused again)

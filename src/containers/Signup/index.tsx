import * as React from "react";
import * as validator from "validator";
import { observer } from "mobx-react-lite";
import { Card } from "../../components/Card";
import { Input } from "../../components/Input";
import { Button } from "../../components/Button";
import { useStores } from "../..";
import { Loading } from "../../components/Loading";
import { Link } from "react-router-dom";

const s = require("./style.scss");

export const SignUp = observer(() => {
  const { authStore } = useStores();
  const [loading, setLoading] = React.useState(false);
  const [errorResponse, setErrorResponse] = React.useState(false);
  const [name, setName] = React.useState("");
  const [errorName, setErrorName] = React.useState("");
  const [email, setEmail] = React.useState("");
  const [errorEmail, setErrorEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [errorPassword, setErrorPassword] = React.useState("");

  const submitForm = async e => {
    e.preventDefault();
    setLoading(true);
    if (!name) {
      setErrorName("Please enter a name");
      setLoading(false);
      return;
    }
    if (!email || !validator.isEmail(email)) {
      setErrorEmail("Please enter a valid email");
      setLoading(false);
      return;
    }
    if (!password || password.length < 7) {
      setErrorPassword("Please enter a valid password");
      setLoading(false);
      return;
    }

    const newUser = { name, email, password };
    const response = await authStore.createUser(newUser);

    if (response.status === "error") {
      setErrorResponse(true);
    }
    setLoading(false);
  };

  const onChangeName = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setErrorName("");
    setName(ev.target.value);
  };

  const onChangeEmail = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setErrorEmail("");
    setEmail(ev.target.value);
  };

  const onChangePassword = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setErrorPassword("");
    setPassword(ev.target.value);
  };

  return (
    <div className={s.signup}>
      <Card>
        <>
          {!errorResponse && !loading && (
            <form className={s.formSignUp} onSubmit={submitForm}>
              <div className={s.inputs}>
                <Input placeholder="Enter your name" onChange={onChangeName} value={name} />
                <label className={s.error}>{errorName}</label>
                <Input placeholder="Enter your email" onChange={onChangeEmail} value={email} />
                <label className={s.error}>{errorEmail}</label>
                <Input placeholder="Enter your password" onChange={onChangePassword} type="password" value={password} />
                <label className={s.error}>{errorPassword}</label>
              </div>
              <Button title="Sign Up" onClick={submitForm} />
              <span className={s.goTo}>
                Go to <Link to="/login">Login</Link>
              </span>
            </form>
          )}
          {!errorResponse && loading && <Loading />}
          {errorResponse && !loading && <div>Something went wrong, please try again.</div>}
        </>
      </Card>
    </div>
  );
});

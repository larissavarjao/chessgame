import * as React from "react";
import { observer } from "mobx-react-lite";
import { Header } from "../../components/Header";
import { useStores } from "../..";
import { Loading } from "../../components/Loading";
import { Button } from "../../components/Button";

const s = require("./style.scss");
const knight = require("../../assets/img/knight.png");
const dot = require("../../assets/img/dot.png");

export const Dashboard = observer(() => {
  const { isLogged, logout } = useStores().authStore;
  const { getMoviments } = useStores().moveStore;
  const [loading, setLoading] = React.useState(false);
  const [errorOnApiCall, setErrorOnApiCall] = React.useState(false);
  const [moves, setMoves] = React.useState<string[]>([]);
  const [currentMove, setCurrentMove] = React.useState("");
  const columns = ["A", "B", "C", "D", "E", "F", "G", "H"];
  const lines = [1, 2, 3, 4, 5, 6, 7, 8];
  const chessBoard = lines.map(num => columns.map(value => value.concat(num.toString()))).reverse();

  const logOut = async () => {
    setLoading(true);
    await logout();
    setLoading(false);
  };

  const onClickPosition = async (move: string) => {
    setLoading(true);
    setCurrentMove(move);
    const resposeCallApi = await getMoviments(move);
    if (resposeCallApi) {
      await setMoves(() => resposeCallApi);
    } else {
      setErrorOnApiCall(true);
    }
    setLoading(false);
  };

  return (
    <div className={s.dashboard}>
      <Header isLogged={isLogged} onLogout={logOut} />
      <div>
        {loading && <Loading />}
        {!loading && (
          <>
            <div className={s.dashboardInfo}>
              <div className={s.row}>
                <div className={s.columns}>
                  {lines.reverse().map(col => (
                    <div className={s.col}>{col}</div>
                  ))}
                </div>
                <div className={s.board}>
                  {chessBoard.map((linesArr, index) => (
                    <div className={index % 2 === 0 ? s.line1 : s.line2}>
                      {linesArr.map(position => (
                        <div className={s.position} onClick={() => onClickPosition(position)}>
                          {position === currentMove && <img className={s.image} src={knight} />}
                          {moves.includes(position) && <img className={s.image} src={dot} />}
                        </div>
                      ))}
                    </div>
                  ))}
                </div>
              </div>
              <div className={s.rows}>
                {columns.map(line => (
                  <div className={s.line}>{line}</div>
                ))}
              </div>
            </div>
            <div>{errorOnApiCall && <label>Something went wrong. Please try again</label>}</div>
          </>
        )}
      </div>
    </div>
  );
});

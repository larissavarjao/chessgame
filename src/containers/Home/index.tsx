import * as React from "react";
import { observer } from "mobx-react-lite";
import { Header } from "../../components/Header";
import { useStores } from "../..";

const s = require("./style.scss");

export const Home = observer(() => {
  const { isLogged } = useStores().authStore;
  return (
    <div className={s.home}>
      <Header isLogged={isLogged} />
      <div className={s.coverHome}>
        <h1 className={s.title}>Chess like you new saw before</h1>
      </div>
    </div>
  );
});

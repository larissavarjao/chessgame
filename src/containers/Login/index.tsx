import * as React from "react";
import * as validator from "validator";
import { observer } from "mobx-react-lite";
import { Card } from "../../components/Card";
import { Input } from "../../components/Input";
import { Button } from "../../components/Button";
import { Link } from "react-router-dom";
import { Loading } from "../../components/Loading";
import { useStores } from "../..";

const s = require("./style.scss");

export const Login = observer(() => {
  const { authStore } = useStores();
  const [loading, setLoading] = React.useState(false);
  const [errorResponse, setErrorResponse] = React.useState(false);
  const [email, setEmail] = React.useState("");
  const [errorEmail, setErrorEmail] = React.useState("");
  const [password, setPassword] = React.useState("");
  const [errorPassword, setErrorPassword] = React.useState("");

  const submitForm = async e => {
    e.preventDefault();
    setLoading(true);
    if (!email || !validator.isEmail(email)) {
      setErrorEmail("Please enter a valid email");
      setLoading(false);
      return;
    }
    if (!password || password.length < 7) {
      setErrorPassword("Please enter a valid password");
      setLoading(false);
      return;
    }

    const response = await authStore.login(email, password);

    if (response.status === "error") {
      setErrorResponse(true);
    }
    setLoading(false);
  };

  const onChangeEmail = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setErrorEmail("");
    setEmail(ev.target.value);
  };

  const onChangePassword = (ev: React.ChangeEvent<HTMLInputElement>) => {
    setErrorPassword("");
    setPassword(ev.target.value);
  };

  return (
    <div className={s.login}>
      <Card>
        <>
          {!errorResponse && !loading && (
            <form className={s.formLogin} onSubmit={submitForm}>
              <div className={s.inputs}>
                <Input placeholder="Enter your email" onChange={onChangeEmail} value={email} />
                <label className={s.error}>{errorEmail}</label>
                <Input placeholder="Enter your password" onChange={onChangePassword} type="password" value={password} />
                <label className={s.error}>{errorPassword}</label>
              </div>
              <Button title="Login" onClick={submitForm} />
              <span className={s.goTo}>
                Go to <Link to="/signup">Sign Up</Link>
              </span>
            </form>
          )}
          {!errorResponse && loading && <Loading />}
          {errorResponse && !loading && <div>Something went wrong, please try again.</div>}
        </>
      </Card>
    </div>
  );
});

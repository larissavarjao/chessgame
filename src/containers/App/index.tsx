import * as React from "react";
import { Switch, Route, Redirect } from "react-router-dom";

import { Home } from "../Home";

import "./style.scss";
import { useStores } from "../..";
import { useObserver } from "mobx-react-lite";
import { SignUp } from "../Signup";
import { Login } from "../Login";
import { Dashboard } from "../Dashboard";

export function App() {
  const { authStore } = useStores();

  const userIsLogged = authStore.isLogged;

  return useObserver(() => {
    if (userIsLogged) {
      return (
        <Switch>
          <Route exact path="/" render={() => <Redirect to="/dashboard" />} />
          <Route path="/dashboard" component={Dashboard} />
        </Switch>
      );
    }
    return (
      <Switch>
        <Route exact path="/" render={() => <Redirect to="/home" />} />
        <Route path="/home" component={Home} />
        <Route path="/signup" component={SignUp} />
        <Route path="/login" component={Login} />
      </Switch>
    );
  });
}

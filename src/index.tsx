import * as React from "react";
import * as ReactDOM from "react-dom";
import { Router } from "react-router-dom";
import { AppContainer } from "react-hot-loader";

import { RootStore } from "./stores";

import { App } from "./containers/App";

if (process.env.NODE_ENV !== "production") {
	console.log("Staging Mode");
}

const rootStore = new RootStore();
const storesCtx = React.createContext(rootStore);

export function useStores() {
	return React.useContext(storesCtx);
}

const render = () => {
	return ReactDOM.render(
		<AppContainer>
			<Router history={rootStore.history}>
				<App />
			</Router>
		</AppContainer>,
		document.getElementById("root"),
	);
};

if (module.hot) {
	module.hot.accept(undefined, render);
}

render();

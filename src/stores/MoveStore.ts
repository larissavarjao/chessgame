import { observable, action } from "mobx";
import * as axios from "axios";
import { api } from "../utils/api";
import { Move } from "../types/move";
import { RootStore } from ".";

export class MoveStore {
  @observable
  public move: Move | null = null;

  private rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;
  }

  @action
  public getMoviments = async (move: string): Promise<string[] | null> => {
    try {
      const response = await axios.default.get(`${api}/moves?moveFrom=${move}`, {
        headers: { Authorization: `Bearer ${this.rootStore.authStore.token}` }
      });
      return response.data.moves;
    } catch (err) {
      console.log("err", err);
      return null;
    }
  };
}

import * as stores from "./";
import { syncHistoryWithStore } from "mobx-react-router";
import * as History from "history";

export class RootStore {
  public routerStore = new stores.RouterStore();
  public history = syncHistoryWithStore(History.createBrowserHistory(), this.routerStore);
  public authStore = new stores.AuthStore(this);
  public moveStore = new stores.MoveStore(this);
}

import { RootStore } from ".";
import { observable, action, computed } from "mobx";
import * as axios from "axios";
import * as qs from "querystring";
import { api } from "../utils/api";

export class AuthStore {
  @observable
  public user: User | null = null;
  public token: string | null = null;

  private rootStore: RootStore;

  constructor(rootStore: RootStore) {
    this.rootStore = rootStore;

    const user: User | null = localStorage.getItem("user") ? JSON.parse(localStorage.getItem("user") as string) : null;
    const token: string | null = localStorage.getItem("token") ? localStorage.getItem("token") : null;

    this.user = user;
    this.token = token;
  }

  @computed
  get isLogged() {
    return !!this.user;
  }

  @action
  public login = async (email: string, password: string): Promise<IResponse<User>> => {
    const resp: IResponse<User> = {
      status: "success",
      message: null,
      data: null
    };

    try {
      const response = await axios.default.request({
        method: "POST",
        url: `${api}/auth`,
        data: qs.stringify({ email, password })
      });
      this.user = response.data.user;
      this.token = response.data.token;

      if (this.user && this.token) {
        this.saveStorage();
        this.saveStorageToken();
        resp.data = this.user;
      }
    } catch (err) {
      console.log("err", err);
      resp.status = "error";
    }
    return resp;
  };

  @action
  public createUser = async (newUser: NewUser): Promise<IResponse<User>> => {
    const resp: IResponse<User> = {
      status: "success",
      message: null,
      data: null
    };

    try {
      const obj = {
        name: newUser.name,
        password: newUser.password,
        email: newUser.email
      };
      const response = await axios.default.request({
        method: "POST",
        url: `${api}/users`,
        data: qs.stringify(obj)
      });
      if (response.status === 201 && response.data) {
        this.user = response.data;
        localStorage.removeItem("user");
        this.login(newUser.email, newUser.password);
        this.saveStorage();
        resp.data = response.data;
      }
    } catch (err) {
      console.log("err", err);
      resp.status = "error";
    }

    return resp;
  };

  @action
  public logout = () => {
    this.localLogOut();
  };

  public saveStorage = () => {
    localStorage.setItem("user", JSON.stringify(this.user as User));
    this.rootStore.history.push("/dashboard");
  };

  public saveStorageToken = () => {
    localStorage.setItem("token", JSON.stringify(this.token as string));
    this.rootStore.history.push("/dashboard");
  };

  @action
  private localLogOut = () => {
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    this.user = null;
    this.token = null;
    this.rootStore.routerStore.push("/");
  };
}

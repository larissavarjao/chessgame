interface IResponse<T> {
  status: "error" | "success";
  message: null | string;
  data: T | null;
}

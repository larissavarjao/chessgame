interface User {
  id: string;
  name: string;
  email: string;
}

interface NewUser {
  name: string;
  email: string;
  password: string;
}

const axios = require("axios");

axios.defaults.baseURL = "https://chessgameapp-api.herokuapp.com";
axios.defaults.headers.common["Authorization"] = ``;
axios.defaults.headers.post["Content-Type"] = "application/x-www-form-urlencoded";

export interface Move {
  id: string;
  moveTo: string;
  moveFrom: string;
}

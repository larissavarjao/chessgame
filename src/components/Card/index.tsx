import * as React from "react";

const s = require("./style.scss");

interface CardProps {
  children: JSX.Element;
}

export function Card({ children }: CardProps) {
  return <div className={s.card}>{children}</div>;
}

import * as React from "react";

const s = require("./style.scss");

interface CardProps extends React.InputHTMLAttributes<HTMLInputElement> {}

export function Input(props: CardProps) {
  return <input className={s.input} {...props} />;
}

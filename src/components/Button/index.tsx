import * as React from "react";

const s = require("./style.scss");

interface IButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  title: string;
}

export function Button(props: IButtonProps) {
  return (
    <button className={s.button} {...props}>
      {props.title}
    </button>
  );
}

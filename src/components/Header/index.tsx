import * as React from "react";
import { Link } from "react-router-dom";
import { Button } from "../Button";

const s = require("./style.scss");
const logo = require("../../assets/img/logo.png");
const knight = require("../../assets/img/knight.png");

interface IHeaderProps {
  onLogout?: () => void;
  isLogged: boolean;
}

export function Header({ onLogout, isLogged }: IHeaderProps) {
  const isMobile = window.innerWidth < 768;

  return (
    <header className={s.header}>
      <Link to="/">
        <img src={isMobile ? knight : logo} className={s.logo} />
      </Link>
      <div className={s.actions}>
        {!isLogged && (
          <Link className={s.about} to="/signup">
            Sign up
          </Link>
        )}
        {!isLogged && (
          <Link className={s.about} to="/login">
            <Button title="Login" />
          </Link>
        )}
        {isLogged && <Button title="Logout" onClick={onLogout} />}
      </div>
    </header>
  );
}

import * as React from "react";
import Loader from "react-loader-spinner";

export function Loading() {
  return <Loader type="Puff" color="#99582a" height="100" width="100" />;
}
